package com.asw;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* This controllers gives some availables fields based on the specified day or specified day and hour
*
* @author  Davide Marsico 
*/

@Configuration
@PropertySource("application.yml")
@RestController
public class GroundController {


	@Value("${grounds}")
	private String grounds;






	@RequestMapping("/{giorno}")
	public String getGrounds(@PathVariable String giorno) {


		String[] wordArray = grounds.split(",");
		int i1 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i2 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i3 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i4 = (int) (Math.round(Math.random()*(wordArray.length-1)));


		// per non estrarre campi uguali
		while(i2 == i1)
			i2 = (int) (Math.round(Math.random()*(wordArray.length-1)));

		// per non estrarre campi uguali
		while(i3 == i2 | i3 == i1)
			i3 = (int) (Math.round(Math.random()*(wordArray.length-1)));

		while(i4 == i1 | i4 == i2 | i4 == i3)
			i4 = (int) (Math.round(Math.random()*(wordArray.length-1)));

		String res = wordArray[i1] + "," +  wordArray[i2] + "," + wordArray[i3] + "," + wordArray[i4] ;

		return res;

	}




	@RequestMapping("/{giorno}/{ora}")
	public String getGroundsOra(@PathVariable String giorno, @PathVariable String ora) {



		String[] wordArray = grounds.split(",");
		int i1 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i2 = (int) (Math.round(Math.random()*(wordArray.length-1)));


		while(i2 == i1)
			i2 = (int) (Math.round(Math.random()*(wordArray.length-1)));


		String res = wordArray[i1] + "," +  wordArray[i2];

		return res;


	}



}
