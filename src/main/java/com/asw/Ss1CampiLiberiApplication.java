package com.asw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss1CampiLiberiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss1CampiLiberiApplication.class, args);
	}
}
